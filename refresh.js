var refreshRate = 60; //Minutes

var timer=setInterval(
  function(){
    refreshFrame()
  },
  refreshRate*60*1000
);

function refreshFrame()
{
  var iframe = document.getElementById('presentFrame');
  var iframeURL = iframe.src;
  iframe.src = iframeURL;
}
