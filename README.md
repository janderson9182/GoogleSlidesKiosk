# Setup
You will need chromium browser installed
```bash
git clone https://github.com/jamesanderson9182/GoogleSlidesKiosk.git
cd GoogleSlidesKiosk
sudo cp startSlides.sh /etc/profile.d #this ensures that it runs on startup, you will need to enter your password and press enter.
sudo vi /etc/profile.d/startSlides.sh # Edit the path in this file to match the location of your html page
```
* Open your Google slides presentation
* File -> Publish to the web...
* Embed -> Slide Size = Large, Select how often you want your slides to advance and tick the two boxes to ensure that the presentation starts automatically and that it starts from the beginning once the presentation gets to the last slide.
* paste the iframe from google slides into slides.html to replace the one that is there
* Add id="presentFrame" to the iframe you pasted in

# What this could be used for
* TV Screen that displays a rolling PowerPoint of information etc.
* You could load this onto a RaspberryPi and if your TV can power the pi, when the TV turns on this will load the presentation automatically.

 # Tags

RaspberryPi | Linux | Google | Google Drive | Google slides | Automation | PowerPoint
